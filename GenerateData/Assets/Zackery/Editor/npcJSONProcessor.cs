﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEditor;

public class NPCJSONProcessor : ScriptableWizard
{
    //Captures data from the 'ItemCollection' class into a private array
    [SerializeField]
    private ItemCollection[] allNPCs;

    //Instatiate variables needed for reading in the json file.
    private string npcJSONString;
    private string npcPath;

    private ItemCollection[] GetAllNPCs()
    {
        if (allNPCs != null)
        {
            return allNPCs;
        }
        else
        {
            Debug.LogError("There are no NPC's.");
            return null;
        }
    }

    //Creates menu item in the engines ribbon of tools. 
    [MenuItem("My Tools / NPC JSON Read Wizard...")]
    static void CreateWizard()
    {
        //Creates the editor tools window title and button name.
        ScriptableWizard.DisplayWizard<NPCJSONProcessor>("Read NPC JSON file", "Read file");
    }

    //When the button is pressed it executes the following within.
    private void OnWizardCreate()
    {
        //Find json - NOTE: change to match file location.
        npcPath = Application.dataPath + "/Zackery/Scripts/npcJSONSW.json";

        //Reads all item json Data.
        npcJSONString = File.ReadAllText(npcPath);

        //Create array from Json.
        ItemCollection[] npcList = JSONHelper.FromJson<ItemCollection>(npcJSONString);
        allNPCs = npcList;

        Debug.Log(npcJSONString);
    }

}