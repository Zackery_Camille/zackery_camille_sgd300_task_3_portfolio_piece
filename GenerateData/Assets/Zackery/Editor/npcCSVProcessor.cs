﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class NPCCSVProccessor : ScriptableWizard
{

    private string[,] csvGrid;

    private string newJsonSW;

    public TextAsset selectCSV;

    static public void DebugOutputGrid(string[,] grid)
    {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++)
        {
            for (int x = 0; x < grid.GetUpperBound(0); x++)
            {
                textOutput += grid[x, y];
                textOutput += "|";
            }

            textOutput += "\n";
        }
        Debug.Log(textOutput);
    }

    private ListOfNPCs GenerateJson()
    {
        //Create new list from the ListOfNPCs class.
        ListOfNPCs newJson = new ListOfNPCs();

        //The following for loop reads through each line of the TextAsset,
        //skipping the column headers and assigns each cell of data,
        //to the corresponding column header.
        for (int i = 1; i < csvGrid.GetLength(1); i++)
        {
            //An item is created only if the line has a uid.
            if (csvGrid[0, i] != null)
            {
                NPCDataTypeList newNPC = new NPCDataTypeList(); // Creates new item
                int.TryParse(csvGrid[0, i], out newNPC.npcStats.globalID);
                newNPC.npcStats.affiliation = csvGrid[1, i];
                newNPC.npcStats.name = csvGrid[2, i];
                newNPC.npcStats.archetype = csvGrid[3, i];
                int.TryParse(csvGrid[4, i], out newNPC.npcStats.health);
                int.TryParse(csvGrid[5, i], out newNPC.npcStats.attackRange);
                int.TryParse(csvGrid[6, i], out newNPC.npcStats.damage);
                int.TryParse(csvGrid[7, i], out newNPC.npcStats.minDamage);
                int.TryParse(csvGrid[8, i], out newNPC.npcStats.maxDamage);
                int.TryParse(csvGrid[9, i], out newNPC.npcStats.speed);
                int.TryParse(csvGrid[9, i], out newNPC.npcStats.armour);
                int.TryParse(csvGrid[10, i], out newNPC.npcStats.exp);
                // Adds the new npc to the npc list.
                newJson.npcs.Add(newNPC);
            }
        }
        // Returns new list of items.
        return newJson;
    }

    //Splits a CSV file into a 2D string array.
    static public string[,] SplitCSVGrid(string csvNPCListText)
    {
        string[] lines = csvNPCListText.Split("\n"[0]);

        //finds the max width of row.
        int width = 0;

        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = SplitCSVLine(lines[i]);
            width = Mathf.Max(width, row.Length);
        }

        //Creates new 2D string grid to output to.
        string[,] outputGrid = new string[width + 1, lines.Length + 1];

        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = SplitCSVLine(lines[y]);

            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];

                //The following line of code is to replace "" with " in the output. 
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }
        return outputGrid;
    }

    //Splits a CSV row.
    static public string[] SplitCSVLine(string line)
    {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                select m.Groups[1].Value).ToArray();
    }


    //Creates menu item in the engines ribbon of tools. 
    [MenuItem("My Tools / Create NPC CSV Wizard...")]
    static void CreateWizard()
    {
        //Creates the editor tools window title and button name.
        ScriptableWizard.DisplayWizard<NPCCSVProccessor>("NPC CSV to JSON conversion", "Generate JSON");
    }

    //When the button is pressed it executes the following within.
    private void OnWizardCreate()
    {
        // Spilts the csv file into a 2D array "csvGrid".
        csvGrid = SplitCSVGrid(selectCSV.ToString());

        // Path that it will save the new json file.
        string path = "Assets/Zackery/Scripts/npcJSONSW.json";

        // Generate json string to be added to new json file.
        string str = JsonUtility.ToJson(GenerateJson());

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }

        // Refresh the unity UI to see new file in folder.
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif

        //The following line below splits the string data from the csv where there is the comma character ',' so,
        //that we can read through the data in the csv.
        string[,] grid = SplitCSVGrid(selectCSV.text);

        Debug.Log("size = " + (1 + grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1)));

        //Calls the DebugOutputGrid Method
        DebugOutputGrid(grid);

    }

}
