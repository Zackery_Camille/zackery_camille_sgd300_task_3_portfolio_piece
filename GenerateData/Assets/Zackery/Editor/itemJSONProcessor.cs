﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEditor;

public class ItemJSONProcessor : ScriptableWizard
{
    //Captures data from the 'ItemCollection' class into a private array
    [SerializeField]
    private ItemCollection[] allItems;

    //Instatiate variables needed for reading in the json file.
    private string itemJsonString;
    private string itemPath;

    private ItemCollection[] GetAllItems()
    {
        if (allItems != null)
        {
            return allItems;
        }
        else
        {
            Debug.LogError("There are no items.");
            return null;
        }
    }

    //Creates menu item in the engines ribbon of tools. 
    [MenuItem ("My Tools / Item JSON Read Wizard...")]
    static void CreateWizard()
    {
        //Creates the editor tools window title and button name.
        ScriptableWizard.DisplayWizard<ItemJSONProcessor>("Read item JSON file", "Read file");
    }

    //When the button is pressed it executes the following within.
    private void OnWizardCreate()
    {
        //Find json - NOTE: change to match file location.
        itemPath = Application.dataPath + "/Zackery/Scripts/itemJSONSW.json";

        //Reads all item json Data.
        itemJsonString = File.ReadAllText(itemPath);

        //Create array from Json.
        ItemCollection[] itemList = JSONHelper.FromJson<ItemCollection>(itemJsonString);
        allItems = itemList;

        Debug.Log(itemJsonString);
    }

}