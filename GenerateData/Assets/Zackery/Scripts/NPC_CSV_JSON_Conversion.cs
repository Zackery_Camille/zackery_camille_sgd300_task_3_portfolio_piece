﻿//Author Dock from 'Unify Community' 10-Jan-2012 [http://wiki.unity3d.com/index.php?title=CSVReader&fbclid=IwAR0tDaOI1OpGKjItw07kASnQprksyk6MNt4swuNH8EMeIJ7BpwBDLla1PcE]
//Author Zackery Camille 28-Aug-2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

/// <summary>
/// The NPC_CSV_JSON_Conversion class sets the 'npcList' csv to a TextAsset. The data from the 'npcList' TextAsset,
/// gets split into a 2D string array. The private method 'ListOfNPCs', creates a new instance of a list called, 
/// 'newJson' from referencing the 'ListOfNPCs' class. The rest of the private method initiates a loop going row, 
/// by row through the data. This action is performed by creating an instance from the 'NPCDataTypeList' class that, 
/// holds all the npc columns data types. Each NPCs data is assigned and parsed through to it's respective column, 
/// and then is added to the 'newJson' list. The loop keeps cycling this process untill all of the data is consumed, 
/// from the TextAsset. Following the end of the loop the the 'newJson' list is then returned.
/// </summary>
public class NPC_CSV_JSON_Conversion : MonoBehaviour
{
    // The 2D string array for the csv file.
    private string[,] csvGrid;

    // The string for the json file for before a new file is created.
    public string newJson;

    //Assign the csv file ('npcList') as a TextAsset type.
    public TextAsset npcList;

    //Outputs the content of a 2D array, useful for checking the importer.
    static public void DebugOutputGrid(string[,] grid)
    {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++)
        {
            for (int x = 0; x < grid.GetUpperBound(0); x++)
            {
                textOutput += grid[x, y];
                textOutput += "|";
            }

            textOutput += "\n";
        }
        Debug.Log(textOutput);
    }

    private ListOfNPCs GenerateJson()
    {
        //Create new list from the ListOfNPCs class.
        ListOfNPCs newJson = new ListOfNPCs();

        //The following for loop reads through each line of the TextAsset,
        //skipping the column headers and assigns each cell of data,
        //to the corresponding column header.
        for (int i = 1; i < csvGrid.GetLength(1); i++)
        {
            //An item is created only if the line has a uid.
            if (csvGrid[0, i] != null)
            {
                NPCDataTypeList newNPC = new NPCDataTypeList(); // Creates new item
                int.TryParse(csvGrid[0, i], out newNPC.npcStats.globalID);
                newNPC.npcStats.affiliation = csvGrid[1, i];
                newNPC.npcStats.name = csvGrid[2, i];
                newNPC.npcStats.archetype = csvGrid[3, i];
                int.TryParse(csvGrid[4, i], out newNPC.npcStats.health);
                int.TryParse(csvGrid[5, i], out newNPC.npcStats.attackRange);
                int.TryParse(csvGrid[6, i], out newNPC.npcStats.damage);
                int.TryParse(csvGrid[7, i], out newNPC.npcStats.minDamage);
                int.TryParse(csvGrid[8, i], out newNPC.npcStats.maxDamage);
                int.TryParse(csvGrid[9, i], out newNPC.npcStats.speed);
                int.TryParse(csvGrid[9, i], out newNPC.npcStats.armour);
                int.TryParse(csvGrid[10, i], out newNPC.npcStats.exp);
                // Adds the new npc to the npc list.
                newJson.npcs.Add(newNPC);
            }
        }
        // Returns new list of NPCs.
        return newJson;
    }

    //Splits a CSV file into a 2D string array.
    static public string[,] SplitCSVGrid(string csvNPCListText)
    {
        string[] lines = csvNPCListText.Split("\n"[0]);

        //finds the max width of row.
        int width = 0;

        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = SplitCSVLine(lines[i]);
            width = Mathf.Max(width, row.Length);
        }

        //Creates new 2D string grid to output to.
        string[,] outputGrid = new string[width + 1, lines.Length + 1];

        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = SplitCSVLine(lines[y]);

            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];

                //The following line of code is to replace "" with " in the output. 
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }
        return outputGrid;
    }

    //Splits a CSV row.
    static public string[] SplitCSVLine(string line)
    {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                select m.Groups[1].Value).ToArray();
    }

    // Start is called before the first frame update.
    private void Start()
    {
        // Spilts the csv file into a 2D array "csvGrid".
        csvGrid = SplitCSVGrid(npcList.ToString());

        // Path that it will save the new json file.
        string path = "Assets/Zackery/Scripts/npcJSON.json";

        // Generate json string to be added to new json file.
        string str = JsonUtility.ToJson(GenerateJson());

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }

        // Refresh the unity UI to see new file in folder.
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif

        //The following line below splits the string data from the csv where there is the comma character ',' so,
        //that we can read through the data in the csv.
        string[,] grid = SplitCSVGrid(npcList.text);

        Debug.Log("size = " + (1 + grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1)));

        //Calls the DebugOutputGrid Method
        DebugOutputGrid(grid);
    }
}

/// <summary>
/// The 'NPCs' class handles creating getters for the npc data types so that they can be accessed in other classes.
/// This is done by creating an new instance of 'NPCDataType' and creating a return statement in a variable for,
/// each npc column.
/// </summary>

public class NPCs
{
    public NPCDataType npcDataType = new NPCDataType();

    public int GetGlobalID()
    {
        return npcDataType.globalID;
    }

    public string GetAffiliation()
    {
        return npcDataType.affiliation;
    }

    public string GetName()
    {
        return npcDataType.name;
    }

    public string GetArchetype()
    {
        return npcDataType.archetype;
    }

    public int GetHealth()
    {
        return npcDataType.health;
    }

    public int GetAttackRange()
    {
        return npcDataType.attackRange;
    }

    public int GetDamage()
    {
        return npcDataType.damage;
    }

    public int GetMinDamage()
    {
        return npcDataType.minDamage;
    }

    public int GetMaxDamage()
    {
        return npcDataType.maxDamage;
    }

    public int GetSpeed()
    {
        return npcDataType.speed;
    }

    public int GetArmour()
    {
        return npcDataType.armour;
    }

    public int GetExp()
    {
        return npcDataType.exp;
    }

}

/// <summary>
/// The 'NPCDataType' class handles initialising all of the data types for the npc columns pertaining to the data,
/// held in the csv and TextAsset.
/// </summary>
[System.Serializable]
public class NPCDataType
{
    public int globalID;
    public string affiliation;
    public string name;
    public string archetype;
    public int health;
    public int attackRange;
    public int damage;
    public int minDamage;
    public int maxDamage;
    public int speed;
    public int armour;
    public int exp;
}

/// <summary>
/// The 'NPCDataTypeList' class is used by setting the NPCs column heading used in the json file to 'npcStats'.
/// </summary>
[System.Serializable]
public class NPCDataTypeList
{
    public NPCDataType npcStats = new NPCDataType();
}

/// <summary>
/// The 'ListOfNPCs' class initialises a list that stores the data from the csv to then be parsed into the json file.
/// </summary>
[System.Serializable]
public class ListOfNPCs
{
    public List<NPCDataTypeList> npcs = new List<NPCDataTypeList>();
}