﻿//Author Dock from 'Unify Community' 10-Jan-2012 [http://wiki.unity3d.com/index.php?title=CSVReader&fbclid=IwAR0tDaOI1OpGKjItw07kASnQprksyk6MNt4swuNH8EMeIJ7BpwBDLla1PcE]
//Author Zackery Camille 28-Aug-2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

/// <summary>
/// The CSV_JSON_Conversion class sets the 'itemList' csv to a TextAsset. The data from the 'itemList' TextAsset,
/// gets split into a 2D string array. The private method 'ListOfItems', creates a new instance of a list called, 
/// 'newJson' from referencing the 'ListOfItems' class. The rest of the private method initiates a loop going row, 
/// by row through the data. This action is performed by creating an instance from the 'ItemList' class that holds, 
/// all the item columns data types. Each item's data is assigned and parsed through to it's respective column and, 
/// then is added to the 'newJson' list. The loop keeps cycling this process untill all of the data is consumed from, 
/// the TextAsset. Following the end of the loop the the 'newJson' list is then returned.
/// </summary>
public class CSV_JSON_Conversion : MonoBehaviour
{
    // The 2D string array for the csv file.
    private string[,] csvGrid;

    // The string for the json file for before a new file is created.
    public string newJson;

    //Assign the csv file ('itemList') as a TextAsset type.
    public TextAsset itemList;

    //Outputs the content of a 2D array, useful for checking the importer.
    static public void DebugOutputGrid(string[,] grid)
    {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++)
        {
            for (int x = 0; x < grid.GetUpperBound(0); x++)
            {
                textOutput += grid[x, y];
                textOutput += "|";
            }

            textOutput += "\n";
        }
        Debug.Log(textOutput);
    }

    private ListOfItems GenerateJson()
    {
        //Create new list from the ListOfItems class.
        ListOfItems newJson = new ListOfItems();

        //The following for loop reads through each line of the TextAsset,
        //skipping the column headers and assigns each cell of data,
        //to the corresponding column header.
        for (int i = 1; i < csvGrid.GetLength(1); i++)
        {
            //An item is created only if the line has a globalID.
            if (csvGrid[0, i] != null)
            {
                // Creates new item
                ItemList newItem = new ItemList();
                int.TryParse(csvGrid[0, i], out newItem.itemStats.globalID);
                int.TryParse(csvGrid[1, i], out newItem.itemStats.catgeoryID);
                newItem.itemStats.categoryName = csvGrid[2, i];
                int.TryParse(csvGrid[3, i], out newItem.itemStats.typeID);
                newItem.itemStats.typeName = csvGrid[4, i];
                int.TryParse(csvGrid[5, i], out newItem.itemStats.sizeX);
                int.TryParse(csvGrid[6, i], out newItem.itemStats.sizeY);
                newItem.itemStats.attackSpeed = csvGrid[7, i];
                int.TryParse(csvGrid[8, i], out newItem.itemStats.DPS);
                int.TryParse(csvGrid[9, i], out newItem.itemStats.minDamage);
                int.TryParse(csvGrid[10, i], out newItem.itemStats.maxDamage);
                newItem.itemStats.levelRequirements = csvGrid[11, i];
                int.TryParse(csvGrid[12, i], out newItem.itemStats.levelRequirementTypeID);
                newItem.itemStats.grip = csvGrid[13, i];
                newItem.itemStats.rarity = csvGrid[14, i];
                int.TryParse(csvGrid[15, i], out newItem.itemStats.value);
                // Adds the new item to the item list.
                newJson.items.Add(newItem);
            }
        }
        // Returns new list of items.
        return newJson;
    }

    //Splits a CSV file into a 2D string array.
    static public string[,] SplitCSVGrid(string csvItemListText)
    {
        string[] lines = csvItemListText.Split("\n"[0]);

        //Finds the max width of row.
        int width = 0;

        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = SplitCSVLine(lines[i]);
            width = Mathf.Max(width, row.Length);
        }

        //Creates new 2D string grid to output to.
        string[,] outputGrid = new string[width + 1, lines.Length + 1];

        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = SplitCSVLine(lines[y]);

            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];

                //The following line of code is to replace "" with " in the output. 
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }
        return outputGrid;
    }

    //Splits a CSV row.
    static public string[] SplitCSVLine(string line)
    {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                select m.Groups[1].Value).ToArray();
    }

    // Start is called before the first frame update.
    private void Start()
    {
        // Spilts the csv file into a 2D array "csvGrid".
        csvGrid = SplitCSVGrid(itemList.ToString());

        // Path that it will save the new json file.
        string path = "Assets/Zackery/Scripts/itemJSON.json";

        // Generate json string to be added to new json file.
        string str = JsonUtility.ToJson(GenerateJson());

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }

        // Refresh the unity UI to see new file in folder.
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif

        //The following line below splits the string data from the csv where there is the comma character ',' so,
        //that we can read through the data in the csv.
        string[,] grid = SplitCSVGrid(itemList.text);

        Debug.Log("size = " + (1 + grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1)));

        //Calls the DebugOutputGrid Method
        DebugOutputGrid(grid);
    }
}

/// <summary>
/// The 'Item' class handles creating getters for the item data types so that they can be accessed in other classes.
/// This is done by creating an new instance of 'ItemDataType' and creating a return statement in a variable for,
/// each item column.
/// </summary>
public class Items
{
    public ItemDataType itemDataType = new ItemDataType();

    public int GetGlobalID()
    {
        return itemDataType.globalID;
    }

    public int GetCategoryID()
    {
        return itemDataType.catgeoryID;
    }

    public string GetCategoryName()
    {
        return itemDataType.categoryName;
    }

    public int GetTypeID()
    {
        return itemDataType.typeID;
    }

    public string GetTypeName()
    {
        return itemDataType.typeName;
    }

    public int GetSizeX()
    {
        return itemDataType.sizeX;
    }

    public int GetSizeY()
    {
        return itemDataType.sizeY;
    }

    public string GetAttackSpeed()
    {
        return itemDataType.attackSpeed;
    }

    public int GetDPS()
    {
        return itemDataType.DPS;
    }

    public int GetMinDamage()
    {
        return itemDataType.minDamage;
    }

    public int GetMaxDamage()
    {
        return itemDataType.maxDamage;
    }

    public string GetLevelRequirements()
    {
        return itemDataType.levelRequirements;
    }

    public int GetLevelRequirementTypeID()
    {
        return itemDataType.levelRequirementTypeID;
    }

    public string GetGrip()
    {
        return itemDataType.grip;
    }

    public string GetRarity()
    {
        return itemDataType.rarity;
    }

    public int GetValue()
    {
        return itemDataType.value;
    }

}

/// <summary>
/// The 'ItemDataType' class handles initialising all of the data types for the item columns pertaining to the data,
/// held in the csv and TextAsset.
/// </summary>
[System.Serializable]
public class ItemDataType
{
    public int globalID;
    public int catgeoryID;
    public string categoryName;
    public int typeID;
    public string typeName;
    public int sizeX;
    public int sizeY;
    public string attackSpeed;
    public int DPS;
    public int minDamage;
    public int maxDamage;
    public string levelRequirements;
    public int levelRequirementTypeID;
    public string grip;
    public string rarity;
    public int value;
}

/// <summary>
/// The 'ItemList' class is used by setting the items column heading used in the json file to 'itemStats'.
/// </summary>
[System.Serializable]
public class ItemList
{
    public ItemDataType itemStats = new ItemDataType();
}

/// <summary>
/// The 'ListOfItems' class initialises a list that stores the data from the csv to then be parsed into the json file.
/// </summary>
[System.Serializable]
public class ListOfItems
{
    public List<ItemList> items = new List<ItemList>();
}