﻿//Author Zackery Camille 04-Sep-2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

/// <summary>
/// The NPC_JSON_Reader handles reading through the 'npcJSON.json' file and then returns it to the console.
/// </summary>
public class NPC_JSON_Reader : MonoBehaviour
{
    //Captures data from the 'NPCCollection' class into a private array
    [SerializeField]
    private NPCCollection[] allNPCs;

    //Instatiate variables needed for reading in the json file.
    string npcJSONString;
    string npcPath;

    /// <summary>
    /// Exception handler to ensure that the 'NPCCollection' class is populated with data. If it is, the 'allNPCs',
    /// array is returned. If there is no data null is returned as well as a 'LogError'.
    /// </summary>
    public NPCCollection[] GetAllNPCs()
    {
        if (allNPCs != null)
        {
            return allNPCs;
        }
        else
        {
            Debug.LogError("There are no NPCs.");
            return null;
        }
    }

    //Start is called before the first frame update.
    void Start()
    {
        //Find json - NOTE: change to match file location.
        npcPath = Application.dataPath + "/Zackery/Scripts/npcJSON.json";

        //Reads all npc json Data.
        npcJSONString = File.ReadAllText(npcPath);

        //Create array from Json.
        NPCCollection[] npcList = NPCJSONHelper.FromJson<NPCCollection>(npcJSONString);
        allNPCs = npcList;

        Debug.Log(npcJSONString);
    }
}

/// <summary>
/// The 'NPCCollection' class handles returning the npc data types Getters from the 'NPCs' class located in the,
/// 'NPC_CSV_JSON_Conversion' script. A new instance of the 'NPCs' class is created to allow each item columns data,
///  type to be referenced and returned.
/// </summary>
[System.Serializable]
public class NPCCollection
{
    public NPCs jrGetNPCDataType = new NPCs();

    public int JRGetGlobalID()
    {
        return jrGetNPCDataType.GetGlobalID();
    }

    public string JRGetAffiliation()
    {
        return jrGetNPCDataType.GetAffiliation();
    }

    public string JRGetName()
    {
        return jrGetNPCDataType.GetName();
    }

    public string JRGetArchetype()
    {
        return jrGetNPCDataType.GetArchetype();
    }

    public int JRGetHealth()
    {
        return jrGetNPCDataType.GetHealth();
    }

    public int JRGetAttackRange()
    {
        return jrGetNPCDataType.GetAttackRange();
    }

    public int JRGetDamage()
    {
        return jrGetNPCDataType.GetDamage();
    }

    public int JRGetMinDamage()
    {
        return jrGetNPCDataType.GetMinDamage();
    }

    public int JRGetMaxDamage()
    {
        return jrGetNPCDataType.GetMaxDamage();
    }

    public int JRGetSpeed()
    {
        return jrGetNPCDataType.GetSpeed();
    }

    public int JRGetArmour()
    {
        return jrGetNPCDataType.GetArmour();
    }

    public int JRGetExp()
    {
        return jrGetNPCDataType.GetExp();
    }

}

public static class NPCJSONHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.items;
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);

    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] items;
    }
}