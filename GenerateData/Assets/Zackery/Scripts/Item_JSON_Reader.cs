﻿//Author Zackery Camille 04-Sep-2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

/// <summary>
/// The Item_JSON_Reader handles reading through the 'itemJSON.json' file and then returns it to the console.
/// </summary>
public class Item_JSON_Reader : MonoBehaviour
{
    //Captures data from the 'ItemCollection' class into a private array
    [SerializeField]
    private ItemCollection[] allItems;

    //Instatiate variables needed for reading in the json file.
    string itemJsonString;
    string itemPath;

    /// <summary>
    /// Exception handler to ensure that the 'ItemCollection' class is populated with data. If it is, the 'allItems',
    /// array is returned. If there is no data null is returned as well as a 'LogError'.
    /// </summary>
    /// <returns></returns>
    public ItemCollection[] GetAllItems()
    {
        if (allItems != null)
        {
            return allItems;
        }
        else
        {
            Debug.LogError("There are no items.");
            return null;
        }
    }

    //Start is called before the first frame update.
    void Start()
    {
        //Find json - NOTE: change to match file location.
        itemPath = Application.dataPath + "/Zackery/Scripts/itemJSON.json";

        //Reads all item json Data.
        itemJsonString = File.ReadAllText(itemPath);

        //Create array from Json.
        ItemCollection[] itemList = JSONHelper.FromJson<ItemCollection>(itemJsonString);
        allItems = itemList;

        Debug.Log(itemJsonString);

    }
}

/// <summary>
/// The 'ItemCollection' class handles returning the item data types Getters from the 'Item' class located in the,
/// 'CSV_JSON_CONVERSION' script. A new instance of the 'Items' class is created to allow each item columns data type,
/// to be referenced and returned.
/// </summary>
[System.Serializable]
public class ItemCollection
{
    public Items jrGetItemDataType = new Items();

    public int JRGetGlobalID()
    {
        return jrGetItemDataType.GetGlobalID();
    }

    public int JRGetCategoryID()
    {
        return jrGetItemDataType.GetCategoryID();
    }

    public string JRGetCategoryName()
    {
        return jrGetItemDataType.GetCategoryName();
    }

    public int JRGetTypeID()
    {
        return jrGetItemDataType.GetTypeID();
    }

    public string JRGetTypeName()
    {
        return jrGetItemDataType.GetTypeName();
    }

    public int JRGetSizeX()
    {
        return jrGetItemDataType.GetSizeX();
    }

    public int JRGetSizeY()
    {
        return jrGetItemDataType.GetSizeY();
    }

    public string JRGetAttackSpeed()
    {
        return jrGetItemDataType.GetAttackSpeed();
    }

    public int JRGetDPS()
    {
        return jrGetItemDataType.GetDPS();
    }

    public int JRGetMinDamage()
    {
        return jrGetItemDataType.GetMinDamage();
    }

    public int JRGetMaxDamage()
    {
        return jrGetItemDataType.GetMaxDamage();
    }

    public string JRGetLevelRequirements()
    {
        return jrGetItemDataType.GetLevelRequirements();
    }

    public int JRGetLevelRequirementTypeID()
    {
        return jrGetItemDataType.GetLevelRequirementTypeID();
    }

    public string JRGetGrip()
    {
        return jrGetItemDataType.GetGrip();
    }

    public string JRGetRarity()
    {
        return jrGetItemDataType.GetRarity();
    }

    public int JRGetValue()
    {
        return jrGetItemDataType.GetValue();
    }

}

public static class JSONHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.items;
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);

    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] items;
    }
}